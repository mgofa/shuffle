// sprintf
if(!String.prototype.format){String.prototype.format=function(){var e=arguments;return this.replace(/{(\d+)}/g,function(t,n){return typeof e[n]!="undefined"?e[n]:t})}}

// settings namespace
window.shuffle = {}

window.shuffle.responsive_start = 940;
window.shuffle.mobile_responsive_start = 450;
window.shuffle.slide_selectors = '#slides, #inception_slider';

function inception_slider_dimensions( selector, existing_slider ) {
   // give inception slider the same width and the outer slider
   selector.width( $( window ).width );
   selector.height( $( window ).height );

   // position the slides-container relative to the inverse of the phones offset
   selector.find( ".slides-container" ).css( "left", -$( "#phone" ).offset().left );
   selector.find( ".slides-container" ).css( "top", -$( "#phone" ).offset().top );
}

var ratio_width_x = 0.72;
var ratio_height_y = 0.45;
var ratio_screenoffset_x = 0.15;
var ratio_screenoffset_y = 0.21;
var ratio_borderradius = 0.07;

function phone_dimensions( vertical_scale ) {
   var aspect_ratio = $( "#phone" ).width() / $( "#phone" ).height();
   var target_height = $( window ).height() * vertical_scale;
   var width = aspect_ratio * target_height;

   $( "#phone, #phone_relative_container" ).css( "width", width ).css( "height", target_height );
   $( "#phone-pic" ).css( "width", width - 1 ).css( "height", target_height - 1 );
   $( "#phone_relative_container" ).css( "border-radius", target_height * ratio_borderradius );

   $( "#screen-slides" ).css( 'left', Math.floor( ratio_screenoffset_x * parseInt( $( "#phone" ).width() ) ) );
   $( "#screen-slides" ).css( 'top', Math.floor( ratio_screenoffset_y * parseInt( $( "#phone" ).height() ) ) );
   $( '#screen-slides li, #screen-slides' ).css( 'width', Math.floor( ratio_width_x * parseInt( $( "#phone" ).width() ) ) );
   $( '#screen-slides li, #screen-slides' ).css( 'height', Math.floor( ratio_height_y * parseInt( $( "#phone" ).height() ) ) );
}

// The superslides animation function needs to be wrapped as there are a number of
// circumstances in which it will fail to transition properly
function goto( index ) {
   if( ( index || index === 0 ) &&  // index must be sane
         index >= 0 && index < window.num_slides && // must be in range of valid slides
         index != window.current_slide &&  // must not be current slide
         ( $( 'body' ).width() > window.shuffle.mobile_responsive_start || window.shuffle.override_mobile_disable ) && // assume that we don't want superslides for mobiles although once the shuffle card animation has fired we actually do
         !window.going ) { // no animation in progress
      $( window.shuffle.slide_selectors ).each( function() {
         $( $( this ).find( "li" )[ index ] ).css( "z-index", "1" );
      } );
      $( window.shuffle.slide_selectors ).superslides( 'animate', index );

      // don't forget to swipe the screen!
      if( window.current_slide < index ) {
         for( var i = window.current_slide; i < index; i++ ) {
            $( window.cards[ i ] ).animate( { 'left': -$( "#screen-slides" ).width(), 'opacity': 0 } ).delay( 400 );
         }
      }
      else {
         for( var i = current_slide - 1; i >= index; i-- ) {
            $( window.cards[ i ] ).animate( { 'left': 0, 'opacity': 1 } ).delay( 400 );
         }
      }

      // update menu
      $( $( "#menu a" ).removeClass( "active" )[ index ] ).addClass("active" );

      if( $( $( "#menu a" )[ index ] ).hasClass( "light" ) ) {
         $( "body" ).removeClass( "dark" ).addClass( "light" );
      } else {
         $( "body" ).removeClass( "light" ).addClass( "dark" );
      }

      // check if our slider has become confused (happens when left on every element is equal)
      setTimeout( function() {
         if( index == 0 ) {
            var slides = $( "#slides > div > ul > li" ).toArray();
            if( slides.map( function( x ) {
               return $( x ).css( "left" );
            } ).reduce( function( x, y ) {
               if( x == y ) { return x; }
            } ) ) {
               var offset = parseInt( $( slides ).first().css( "left" ).replace( "px", "" ) );
               slides.slice( 1 ).map( function( x ) {
                  $( x ).css( "left", 2 * offset + "px" );
               } );
            }
         }
      }, 1000 );

      window.current_slide = index;
      return true;
   } else {
      return false;
   }
}

function calculate_opacity( dx ) {
   return 1 / ( Math.abs( dx ) / ( window.dx / 0.75 ) + 1 );
}

$( document ).ready( function() {
   $( window.shuffle.slide_selectors ).superslides( { pagination: false } );

   // partially clear the default superslides styles and recalculate #phone dimensions
   $( "#screen-slides" ).attr( "style", "position: relative; overflow: hidden;" );
   phone_dimensions( 0.75 );

   setInterval( function() {
      if( typeof( window.dx ) == 'undefined' || typeof( window.dy ) == 'undefined' || window.dx != $( window ).width() || window.dy != $( window ).height() ) {
         inception_slider_dimensions( $( "#inception_slider" ), $( "#slides" ) );    
         phone_dimensions( 0.75 );
         window.dx = $( window ).width();
         window.dy = $( window ).height();     
      }
   }, 1000 );

   // menu
   $( "body > #menu a" ).click( function( event ) {
      var index = parseInt( $( this ).attr( "href" ).replace( "#", "" ) );
      if( goto( index ) ) {
         $( "#menu a" ).removeClass( "active" );
         $( this ).addClass( "active" );
      }
      event.preventDefault();
      event.stopPropagation();
   } );

   /* responsive menu */

   // get an idea of potential height for dropdown
   $( document ).ready( function() {
      if( $( 'body' ).width() <= window.shuffle.responsive_start ) {
         $( "#menu" ).css( "height", "auto" );
         window.shuffle.responsive_menu_height = $( "#menu" ).height();
         $( "#menu" ).css( "height", "0px" );         
      } else {
         window.shuffle.responsive_menu_height = "auto";
      }
   } );
   $( "#responsive-menu-toggle" ).on( "click touchend", function( event ) {
      if( $( "#menu" ).height() <= 0 ) {
         // if height is auto then we weren't able to get an accurate height (as
         // indicated by it being too small to be appropriate). Therefore, set height
         // to auto and make note of the generated height to see if we can use it
         // for the next touch event.
         if( window.shuffle.responsive_menu_height == "auto" ) {
            $( "#menu" ).css( "height", "auto" );
            window.shuffle.responsive_menu_height = $( "#menu" ).height();
         } else {
            $( "#menu" ).animate( { "height": window.shuffle.responsive_menu_height } );
         }
      } else {
         $( "#menu" ).animate( { "height": "0px" } );
      }
      event.preventDefault();
      event.stopPropagation();
   } );

   // look for touch outside menu
   $( 'body' ).on( "click touchmove", function( event ) {
      if( $( "#menu" ).height() > 0 ) {
         $( "#responsive-menu-toggle" ).trigger( $.Event( "click" ) );
         event.preventDefault();
         event.stopPropagation();
      }
   } );

   // functions to control nav
   window.going = 0;
   $( window.shuffle.slide_selectors ).on( 'animating.slides', function( event ) {
      window.going++;
   } );
   $( window.shuffle.slide_selectors ).on( 'animated.slides', function( event ) {
      if( window.going > 0 ) {
         window.going--;
      }
   } );
   window.goleft = function( x ) {
      if( window.current_slide + 1 < window.num_slides && !window.going ) {
         goto( window.current_slide + 1 );
      }
   };
   window.goright = function( x ) {
      if( window.current_slide > 0 && !window.going ) {
         goto( window.current_slide - 1 );
      }
   };

   // handle swipe events
   if( typeof( Hammer ) !== 'undefined' ) {
      window.current_slide = 0;
      window.num_slides = $( window.shuffle.slide_selectors ).first().find( "li" ).length;
      window.mc = new Hammer( $( "body" )[ 0 ] );
      window.mc.on( 'swipeleft', window.goleft );
      window.mc.on( 'swiperight', window.goright );
   }

   // handle left/right keypress
   $( document ).keydown( function( e ) {
      switch( e.which ) {
         case 37: // left
         case 38: // up
            window.goright( true );
            break;
         case 39: // right
         case 40: // down
            window.goleft( true );
            break;
         default: return;
      }
   });

   /* Cards (mobile only) */
   window.shuffle.override_mobile_disable = false;
   window.shuffle.slide_threshold = 120;
   window.current_card = 0;
   window.cards = $( "#screen-slides img" );
   for( var i = 0; i < window.cards.length; i++ ) {
      $( window.cards[ i ] ).css( "z-index", 1000 - i ).attr( "id", "card_" + i );
   }

   // mobile draggable
   window.mhr = $( "#draggable" );
   mobile_hammer = new Hammer( window.mhr[ 0 ] );
   mobile_hammer.add( new Hammer.Pan( { threshold: 0, pointers: 0 } ) );
   mobile_hammer.add( new Hammer.Swipe() ).recognizeWith( mobile_hammer.get( 'pan' ) );

   // translate position based on delta
   mobile_hammer.on( 'pan', function( ev ) {
      window.mhr.css( "transform", "translate3d({0}px, {1}px, 0px)".format( ev.deltaX, ev.deltaY ) ).css( "opacity", calculate_opacity( ev.deltaX ) ); 
   } );

   // disable animation -- this thing's on manual now!
   mobile_hammer.on( 'panstart', function( ev ) {
      window.mhr.attr( "style", "" ).removeClass( 'enable_animation' );
   } );

   // animate back to 0,0,0
   mobile_hammer.on( 'panend', function( ev ) {
      // user has indicated intention to move to the 'next' slide
      if( Math.abs( ev.deltaX ) > window.shuffle.slide_threshold ) {
         $( '.slide-home .slide.regular' ).css( 'display', 'block' );
         var direction = ( ev.deltaX > 0 ) ? 1 : -1;
         $( '.mobile_overlay' ).animate( { 'left': direction * $('body').width(), 'opacity': 0 }, 400, function() {
            $( this ).css( 'display', 'none' );
            $( '.mobile_logo' ).removeClass( 'hidden' );
            window.shuffle.override_mobile_disable = true;
            $( "#responsive-menu-toggle" ).css( "display", "block" );
         } );
      } else {
         window.mhr.addClass( 'enable_animation' ).css( 'transform', 'translate3d( 0px, 0px, 0px )' ).delay( '500' ).attr( "style", "" );
      }
   } );

   /* Registration */

   function register( email, name ) {
      if( typeof( name ) === 'undefined' ) {
         name = '';
      }
      _cio.identify( {
         'id': email,
         'email': email,
         'created_at': new Date().getTime(),
         'name': name,
      } );
      $( "#footer" ).html( '<div class="register success"><p>Registration Successful</p></div>' );
      setTimeout( function() {
         $( "#footer" ).slideUp();
      }, 100 * 30 )
   }

   // Email
   $( "#email_signup" ).submit( function( event ) {
      if( $( ".register.email.responsive" ).css( "display" ) == "none" ) {
         register( $( ".register.email.desktop" ).val() );
      } else {
         register( $( ".register.email.responsive" ).val() );
      }
      event.preventDefault();
   } );

   /* Video Popover */
   $( '.popup-youtube' ).magnificPopup( {
      disableOn: 700,
      type: 'iframe',
      mainClass: 'mfp-fade',
      removalDelay: 160,
      preloader: false,
      fixedContentPos: false
   } );

   /* Magnific */
   $( '.iframe.download-btn' ).click( function( event ) {
      event.preventDefault();
      $.magnificPopup.open( {
         items: {
            src: $( event.target ).attr( 'href' )
         },
         type: 'iframe'
      } );
   } );

   /* Yet Another Slider */

   var slideCount = $( '.featureslider ul li' ).length;
   var slideWidth = $( '.featureslider ul li' ).width();
   var slideHeight = $( '.featureslider ul li' ).height();
   var sliderUlWidth = slideCount * slideWidth;

   $( '.featureslider' ).css( { width: slideWidth } );

   $( '.featureslider ul' ).css( { width: sliderUlWidth, marginLeft: - slideWidth } );

   $( '.featureslider ul li:last-child' ).prependTo('.featureslider ul');

   $( 'a.control_prev' ).click(function () {
      $( '.featureslider ul' ).animate( {
         left: + $( '.featureslider ul li' ).width()
      }, function () {
         $( '.featureslider ul li:last-child' ).prependTo( '.featureslider ul' );
         $( '.featureslider ul' ).css( 'left', '' );
      } );
      event.preventDefault();
   } );

   $( 'a.control_next').click(function () {
      $( '.featureslider ul').animate( {
         left: - $( '.featureslider ul li' ).width()
      }, function () {
         $( '.featureslider ul li:first-child' ).appendTo( '.featureslider ul' );
         $( '.featureslider ul' ).css( 'left', '' );
      } );
      event.preventDefault();
   } );

   setInterval( function () {
      var default_size = slideWidth;
      if( window.dx < default_size ) { // moved to mobile -- resize
         $( ".featureslider ul" ).css( "margin-left", "-" + window.dx + "px" ).find( "li" ).width( window.dx );      
      } else if( window.dx >= default_size && $( ".featureslider ul li" ).width() < default_size ) { // moved from mobile to regular -- resize
         $( ".featureslider ul" ).css( "margin-left", "-" + default_size + "px" ).find( "li" ).width( default_size );      
      }
   }, 1000 );
} );