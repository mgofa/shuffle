module.exports = function( grunt ) {
  js_build_files = [
    'script/reduce.polyfill.js',
    'script/map.polyfill.js',
    'script/hammer.min.js',
    'script/jquery.animated-enhanced.min.js',
    'script/jquery.superslides.min.js',
    'script/popup.min.js',
    'script/main.js'
  ];
  grunt.initConfig( {
    uglify: {
      options: {
        mangle: false,
        sourceMap: true
      },
      my_target: {
        files: { 'script/bundle.min.js': js_build_files }
      }
    },
    watch: {
      styles: {
        files: [ 'style/main.scss' ],
        tasks: [ 'default' ],
        options: {
          spawn: false,
        }
      },
      scripts: {
        files: js_build_files,
        tasks: [ 'default' ],
        options: {
          spawn: false
        }
      }
    },
    sass: {
      dist: {
        options: {
          style: 'compressed'
        },
        files: [ {
          expand: true,
          cwd: 'style',
          src: [ 'main.scss' ],
          dest: 'style',
          ext: '.min.css'
        } ]
      }
    }
  } );
  grunt.loadNpmTasks( 'grunt-contrib-uglify' );
  grunt.loadNpmTasks( 'grunt-contrib-watch' );
  grunt.loadNpmTasks( 'grunt-contrib-sass' );
  grunt.registerTask( 'default', [ 'sass', 'uglify' ] );
};